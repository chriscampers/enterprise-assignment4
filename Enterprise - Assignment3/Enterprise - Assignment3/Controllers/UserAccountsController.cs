﻿using Enterprise___Assignment3.Domain.Entities;
using Enterprise___Assignment3.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enterprise___Assignment3.Controllers
{
    public class UserAccountsController : Controller
    {  
        private const string USER_SESSION_OBJ = "USER";
        IUserRepository _userAccounts;
        public User User
        {
            get
            {
                User user = Session[USER_SESSION_OBJ] as User;
                if (user == null)
                {
                    user = new User();
                    Session[USER_SESSION_OBJ] = user;
                }
                return user;
            }            
        }
        // GET: UserAccounts
        public UserAccountsController(IUserRepository userAccounts)
        {
            _userAccounts = userAccounts;
        }
        [HttpGet]
        public ActionResult Index()
        {
            IUserRepository userl;
            
            return View();
        }
        [HttpPost]
        public ViewResult Index(User user, string submit)
        {
            User u = _userAccounts.FindUser(user);
            if(u == null)
            {
                ViewBag.FailedLogin = "Username or Password Incorrect Please try again";
                return View();
            }
            else
            {
                Session[USER_SESSION_OBJ] = u;
                return View("UserView", u);
            }
            return View();
        }
    }
}