﻿using Enterprise___Assignment3.Domain.Entities;
using System.Collections.Generic;

namespace Assignment4.Domain.Persistence
{
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> InvoiceList { get; }
        Invoice FindInvoice(int invoiceNum);
        void AddInvoice(Invoice invoice);
        void UpdateInvoice(Invoice invoice);
        void PayInvoice(Invoice invoice);

    }
}