﻿using System.Threading.Tasks;
using System.Data.Entity;
using Enterprise___Assignment3.Domain.Entities;

namespace Enterprise___Assignment3.Domain.Persistence
{
    class Assignment4DbContext : DbContext
    {
        public Assignment4DbContext() : base("Assignment4DbConnection")
        {

        }
        public DbSet<User> User { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
    }
}
