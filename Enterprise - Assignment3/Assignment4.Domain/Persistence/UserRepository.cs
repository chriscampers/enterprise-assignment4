﻿using Enterprise___Assignment3.Domain;
using Enterprise___Assignment3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise___Assignment3.Domain.Persistence
{
    public class UserRepository : IUserRepository
    {
        private Assignment4DbContext context;

        public UserRepository()
        {
            context = new Assignment4DbContext();
        }
        public IEnumerable<User> UserList
        {
            get { return context.User; }
        }
        public User FindUser(User user)
        {
            foreach(User u in UserList)
            {
                if (u.UserName.Equals(user.UserName))
                {
                    return (u);
                }
                else
                {
                    
                }
            }
            return null;
        }
        public void SaveUser(User user)
        {

            //change the product IN the database. How?
            //obtain the product FROM the database
            User userEntity = context.User.Find(user.UserName);

            //change the product entity itself
            userEntity.Change(user);

           
            //apply the changes (add or update) to the database
            context.SaveChanges();


        }
    }
}
