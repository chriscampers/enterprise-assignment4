﻿using Enterprise___Assignment3.Domain.Entities;
using Enterprise___Assignment3.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.Domain.Persistence
{
    class InvoiceRepository: IInvoiceRepository
    {
        
        private Assignment4DbContext context;

        public InvoiceRepository()
        {
            context = new Assignment4DbContext();
        }
        public IEnumerable<Invoice> InvoiceList
        {
            get { return context.Invoice; }
        }
        public void UpdateInvoice(Invoice invoice)
        {
            Invoice tempI = context.Invoice.Find(invoice.InvoiceNumber);
            tempI.Update(invoice);
            context.SaveChanges();
        }
        public void PayInvoice(Invoice invoice)
        {
            Invoice tempI = context.Invoice.Find(invoice.InvoiceNumber);
            tempI.Pay(invoice);
            context.SaveChanges();
        }
        public void AddInvoice(Invoice invoice)
        {
            context.Invoice.Add(invoice);
            context.SaveChanges();
        }

        public Invoice FindInvoice(int invoiceNum)
        {
            throw new NotImplementedException();
        }
    }

}
