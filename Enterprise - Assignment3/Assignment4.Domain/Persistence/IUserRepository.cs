﻿using Enterprise___Assignment3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise___Assignment3.Domain.Persistence
{

    public interface IUserRepository
    {
        IEnumerable<User> UserList { get; }

        void SaveUser(User user);
        User FindUser(User user);

        //TODO: add method for deleting a product from the catalog
    }
}
