﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Domain.Entities
{
    public enum Currency
    {
        CAD,
        US,
        EUR
    }
    public class Invoice
    {
     
        public Invoice()
        {
            IsPaid = false;
        }
        [Required(ErrorMessage = "Please Enter Clients Name")]
        public string ClientName { get; set; }

        [Required(ErrorMessage = "Please Enter Clients Address")]
        public string ClientAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Date of Shipment")]
        public DateTime DateofShipment { get; set; }

        [Required(ErrorMessage = "Please Enter Payments Due Date")]
        public DateTime PaymentDueDate { get; set; }

        [Required(ErrorMessage = "Please Enter your Product Name")]
        public string ProductName { get; set; }



        [Required(ErrorMessage = "Please Enter Product Quantity")]
        public int ProductQuantity { get; set; }

        [Required(ErrorMessage = "Please Enter Unit Price")]
        public decimal UnitPrice { get; set; }

        [Required(ErrorMessage = "Please Enter Currency Type")]
        public Currency Currency { get; set; }
        [Key]
        public int InvoiceNumber { get; set; }
        
        public bool IsPaid { get; set; }
        public double TotalPrice()
        {
            
            return Math.Round(ProductQuantity * (double)UnitPrice, 2);
        }
        public void Update(Invoice invoice)
        {
            this.ClientAddress = invoice.ClientAddress;
            this.ClientName = invoice.ClientName;
            this.Currency = invoice.Currency;
            this.DateofShipment = invoice.DateofShipment;
            this.InvoiceNumber = invoice.InvoiceNumber;
            this.PaymentDueDate = invoice.PaymentDueDate;
            this.ProductName = invoice.ProductName;
            this.ProductQuantity = invoice.ProductQuantity;
            this.UnitPrice = invoice.UnitPrice;
        }
        public void Pay(Invoice invoice)
        {
            this.IsPaid = true;
        }
    }
}