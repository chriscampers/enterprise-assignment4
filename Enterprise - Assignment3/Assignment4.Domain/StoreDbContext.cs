﻿using Enterprise___Assignment3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Domain
{

    class StoreDbContext : DbContext
    {
        public StoreDbContext() : base("Assignment4DbConnection")
        {
        }

        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
