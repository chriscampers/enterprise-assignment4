﻿using Assignment4.Domain.Persistence;
using Enterprise___Assignment3.Domain.Persistence;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Domain
{

    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {
           // Bind<IProductCatalog>().To<ProductCatalog>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IInvoiceRepository>().To<InvoiceRepository>();
        }
    }
}